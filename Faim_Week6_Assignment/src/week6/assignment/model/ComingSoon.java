package week6.assignment.model;

import java.sql.SQLException;
import java.util.List;

import week6.assignment.database.ComingSoonDatabase;
import week6.assignment.factory.Movies;

public class ComingSoon extends Movies{
	
	private int id;
	private String title;
	private int year;
	private String genre;
	private String contentRating;
	private String duration;
	private String releaseDate;
	private String actors;
	private String imdbRating;
	
	@Override
	public void getMovies() {
		
		ComingSoonDatabase comingSoonDatabase = new ComingSoonDatabase();
		try {
			List<ComingSoon> listOfComingSoonMovies = comingSoonDatabase.getAllComingSoonMovies();
			
			for(ComingSoon comingSoon : listOfComingSoonMovies) {
				System.out.println(comingSoon);
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public ComingSoon() {
		
	}

	public ComingSoon(int id, String title, int year, String genre, String contentRating, String duration,
			String releaseDate, String actors, String imdbRating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.contentRating = contentRating;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.actors = actors;
		this.imdbRating = imdbRating;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	@Override
	public String toString() {
		return "ComingSoon [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + ", contentRating="
				+ contentRating + ", duration=" + duration + ", releaseDate=" + releaseDate + ", actors=" + actors
				+ ", imdbRating=" + imdbRating + "]";
	}
	
	
	
}
