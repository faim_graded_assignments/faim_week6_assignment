package week6.assignment.model;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import week6.assignment.database.ComingSoonDatabase;
import week6.assignment.database.FavoriteMoviesDatabase;
import week6.assignment.database.TheaterMoviesDatabase;
import week6.assignment.factory.Movies;

public class Favorites extends Movies{
	
	private int id;
	private String title;
	private int year;
	private String genre;
	private String contentRating;
	private String duration;
	private String releaseDate;
	private String actors;
	private String imdbRating;
	
	@Override
	public void getMovies() {
		FavoriteMoviesDatabase favoriteMoviesDatabase = new FavoriteMoviesDatabase();
		try {
			List<Favorites> listOfFavoriteMovies = favoriteMoviesDatabase.getAllFavoriteMovies();
			if(listOfFavoriteMovies.isEmpty()) {
				System.out.println("Your Favorites List is Empty");
			}else {
				for(Favorites favorites : listOfFavoriteMovies) {
					System.out.println(favorites);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	Scanner sc = new Scanner(System.in);
	
	public void addMoviesToFavorites() {
		
		while(true) {
			System.out.println("Enter the Choice");
			System.out.println("1 => Add Movies from Coming Soon Movies");
			System.out.println("2 => Add Movies from Theater Movies");
			System.out.println("3 => Show All Favorite Movies");
			System.out.println("0 => To Exit the Add Function");

			int addMovieChoice = sc.nextInt();
			
			if(addMovieChoice == 0) {
				break;
			}
			
			switch (addMovieChoice) {
			case 1:
				ComingSoonDatabase comingSoonDatabase = new ComingSoonDatabase();
				try {
					List<ComingSoon> listOfAllUpComingMovies = comingSoonDatabase.getAllComingSoonMovies();
					for(ComingSoon comingSoon : listOfAllUpComingMovies) {
						System.out.println(comingSoon);
					}
//					System.out.println("Enter the Movies Id To add To Favorites");
//					int movieId = sc.nextInt();
					System.out.println("Enter the Movies Title from above movies");
					String movieTitle = sc.next();
					
					FavoriteMoviesDatabase favoriteMoviesDatabase = new FavoriteMoviesDatabase();
					boolean isPresentInComingSoonAdd = false;
					boolean isPresent = favoriteMoviesDatabase.checkMovieInFavorites(movieTitle);
					if(isPresent) {
						System.out.println("Movie Already is in favorites");
					}else {
						isPresentInComingSoonAdd = favoriteMoviesDatabase.selectMoviesFromComingSoon(movieTitle);
					}
					if(isPresentInComingSoonAdd) {
						System.out.println("Movie Present in comingsoon");
						getMovies();
					}else {
						System.out.println("Movies Not added");
					}
					
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 2:
				TheaterMoviesDatabase theaterMoviesDatabase = new TheaterMoviesDatabase();
				List<TheaterMovies> listOfTheaterMovies;
				
				try {
					listOfTheaterMovies = theaterMoviesDatabase.getAllTheaterMovies();
					for(TheaterMovies theaterMovies : listOfTheaterMovies) {
						System.out.println(theaterMovies);
					}
					
//					System.out.println("Enter the Movie Id To add To Favorites");
//					int movieId = sc.nextInt();
					System.out.println("Enter the Movies Title from above movies");
					String movieTitle = sc.next();
					
					FavoriteMoviesDatabase favoriteMoviesDatabase = new FavoriteMoviesDatabase();
					boolean isPresentInTheaterAdd = false;
					boolean isPresent = favoriteMoviesDatabase.checkMovieInFavorites(movieTitle);
					if(isPresent) {
						System.out.println("Movie Already is in favorites");
					}else {
						isPresentInTheaterAdd = favoriteMoviesDatabase.selectMoviesFromTheater(movieTitle);
					}
					if(isPresentInTheaterAdd) {
						getMovies();
					}else {
						System.out.println("Movies Not added");
					}
					
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
				break;
				case 3:
					getMovies();
					break;
				
			default:
				System.out.println("Invalid case");
				break;
			}
		}
	}
	
	public boolean deleteFavoriteMovies(){
		//Showing all movies using getMovies() method
		getMovies();
		
		System.out.println("Enter the Movies Title from above movies");
		String movieTitle = sc.next();
		
		try {
			FavoriteMoviesDatabase favoriteMoviesDatabase = new FavoriteMoviesDatabase();
			boolean isPresentInFavorites = false;

			isPresentInFavorites = favoriteMoviesDatabase.deleteMoviesFromFavorites(movieTitle);
			if(isPresentInFavorites) {
				return true;
			}else {
				return false;
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	//Constructor
	public Favorites() {
		
	}
	
	public Favorites(int id, String title, int year, String genre, String contentRating, String duration,
			String releaseDate, String actors, String imdbRating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.contentRating = contentRating;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.actors = actors;
		this.imdbRating = imdbRating;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	@Override
	public String toString() {
		return "Favorites [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + ", contentRating="
				+ contentRating + ", duration=" + duration + ", releaseDate=" + releaseDate + ", actors=" + actors
				+ ", imdbRating=" + imdbRating + "]";
	}
}
