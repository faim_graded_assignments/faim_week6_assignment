package week6.assignment.model;

import java.sql.SQLException;
import java.util.List;

import week6.assignment.database.TheaterMoviesDatabase;
import week6.assignment.factory.Movies;

public class TheaterMovies extends Movies{

	private int id;
	private String title;
	private int year;
	private String genre;
	private String content_rating;
	private String duration;
	private String release_date;
	private String actors;
	private String imdb_rating;
	
	public TheaterMovies() {

	}
	
	@Override
	public void getMovies(){
		TheaterMoviesDatabase theaterMoviesDatabase = new TheaterMoviesDatabase();
		try {
			List<TheaterMovies> listOftheaterMovies = theaterMoviesDatabase.getAllTheaterMovies();
			
			for(TheaterMovies theaterMovies : listOftheaterMovies) {
				System.out.println(theaterMovies);
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public TheaterMovies(int id, String title, int year, String genre, String content_rating, String duration,
			String release_date, String actors, String imdb_rating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.content_rating = content_rating;
		this.duration = duration;
		this.release_date = release_date;
		this.actors = actors;
		this.imdb_rating = imdb_rating;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getContent_rating() {
		return content_rating;
	}

	public void setContent_rating(String content_rating) {
		this.content_rating = content_rating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getImdb_rating() {
		return imdb_rating;
	}

	public void setImdb_rating(String imdb_rating) {
		this.imdb_rating = imdb_rating;
	}

	@Override
	public String toString() {
		return "TheaterMovies [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre
				+ ", content_rating=" + content_rating + ", duration=" + duration + ", release_date=" + release_date
				+ ", actors=" + actors + ", imdb_rating=" + imdb_rating + "]";
	}

}
