package week6.assignment.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	static Connection conn = null;
	
	public static Connection getConnection(){
		
		
		try {
			if(conn == null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies","root","root");
			}
		} catch (ClassNotFoundException e) {
			System.out.println("error driver not loaded");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("cannot connect to database");
			e.printStackTrace();
		}
		return conn;
	}

}
