package week6.assignment.testcases;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import week6.assignment.database.ComingSoonDatabase;
import week6.assignment.database.FavoriteMoviesDatabase;
import week6.assignment.database.TheaterMoviesDatabase;
import week6.assignment.factory.Movies;
import week6.assignment.factory.MoviesFactory;
import week6.assignment.model.ComingSoon;
import week6.assignment.model.Favorites;
import week6.assignment.model.TheaterMovies;

class JunitTestCases {

	
	private int id = 5;
	private String name = "Black Panther";
	private int year;
	private String genre;
	private String contentRating;
	private String duration;
	private String releaseDate;
	private String actors;
	private String imdb;
	private String movieTitle;
	
	
	@BeforeEach
	public void beforeEach() {
		id = 5;
		name = "Black Panther";
		year = 2018;
		genre = "Action";
		contentRating = "89%";
		duration = "PT97M";
		releaseDate = "2018-05-02";
		actors = "Chadwick Boseman";
		imdb = "7.9";
	}
	
	@AfterEach
	public void afterEach() {
		id = 0;
		name = null;
		year = 0;
		genre = null;
		contentRating = null;
		duration = null;
		releaseDate = null;
		actors = null;
		imdb = null;
		movieTitle = "Aiyaary";
	}
	

	
	// Test Case for all getters and setters for the ComingSoonMovies class
	@Test
	public void getterAndSetterForComingSoonTest() {
//		ComingSoon comingSoon = new ComingSoon();
		ComingSoon comingSoon = (ComingSoon) MoviesFactory.getTheMovies(1);
		
		comingSoon.setId(id);
		comingSoon.setActors(actors);
		comingSoon.setYear(year);
		comingSoon.setTitle(name);
		comingSoon.setDuration(duration);
		comingSoon.setGenre(genre);
		comingSoon.setReleaseDate(releaseDate);
		comingSoon.setContentRating(contentRating);
		comingSoon.setImdbRating(imdb);
		
		assertTrue(comingSoon.getId() == id);
		assertTrue(comingSoon.getTitle().equals(name));
		assertTrue(comingSoon.getActors().equals(actors));
		assertTrue(comingSoon.getYear() == year);
		assertTrue(comingSoon.getDuration().equals(duration));
		assertTrue(comingSoon.getGenre().equals(genre));
		assertTrue(comingSoon.getReleaseDate().equals(releaseDate));
		assertTrue(comingSoon.getImdbRating().equals(imdb));
	}
	
	// Test Case for Getters and Setters for TheaterMovies class
	
	@Test
	public void gettersAndSettersForTheaterMovies() {
		TheaterMovies theaterMovies = (TheaterMovies) MoviesFactory.getTheMovies(2);
		
		theaterMovies.setId(id);
		theaterMovies.setTitle(name);
		theaterMovies.setYear(year);
		theaterMovies.setGenre(genre);
		theaterMovies.setContent_rating(contentRating);
		theaterMovies.setDuration(duration);
		theaterMovies.setRelease_date(releaseDate);
		theaterMovies.setActors(actors);
		theaterMovies.setImdb_rating(imdb);
		
		assertFalse(theaterMovies.getId() != id);
		assertFalse(!theaterMovies.getTitle().equals(name));
		assertFalse(theaterMovies.getYear() != year);
		assertFalse(!theaterMovies.getGenre().equals(genre));
		assertFalse(!theaterMovies.getContent_rating().equals(contentRating));
		assertFalse(!theaterMovies.getDuration().equals(duration));
		assertFalse(!theaterMovies.getRelease_date().equals(releaseDate));
		assertFalse(!theaterMovies.getActors().equals(actors));
		assertFalse(!theaterMovies.getImdb_rating().equals(imdb));
		
	}
	
	
	// Test for FavoritesMovies Class
	@Test
	public void gettersAndSettersForFavoriteMovies() {
		Favorites favorites = (Favorites) MoviesFactory.getTheMovies(3);
		
		favorites.setId(id);
		favorites.setTitle(name);
		favorites.setYear(year);
		favorites.setGenre(genre);
		favorites.setContentRating(contentRating);
		favorites.setDuration(duration);
		favorites.setReleaseDate(releaseDate);
		favorites.setActors(actors);
		favorites.setImdbRating(imdb);
		
		assertEquals(favorites.getId(),id);
		assertEquals(favorites.getTitle(),name);
		assertEquals(favorites.getYear(),year);
		assertEquals(favorites.getGenre(),genre);
		assertEquals(favorites.getContentRating(),contentRating);
		assertEquals(favorites.getDuration(),duration);
		assertEquals(favorites.getReleaseDate(),releaseDate);
		assertEquals(favorites.getImdbRating(),imdb);
		
	}
	
	// Test Case for ComingSoonMovies Class List
	
	@Test
	public void comingSoonDatabaseTest() {
		
		ComingSoonDatabase comingSoonDatabase = new ComingSoonDatabase();
		try {
			assertFalse(comingSoonDatabase.getAllComingSoonMovies().isEmpty());
		} catch (SQLException e) {
			System.out.println("Junit Method Throws Some Error with the Message " + e.getMessage());
		}
	}
	
	// Test Case for Theater Movies Class List
	
	@Test
	public void theaterMoviesDatabaseTest() {
		
		TheaterMoviesDatabase theaterMoviesDatabase = new TheaterMoviesDatabase();
		try {
			assertTrue(!theaterMoviesDatabase.getAllTheaterMovies().isEmpty());
		} catch (SQLException e) {
			System.out.println("Junit Method Throws Some Error with the Message " + e.getMessage());
		}
	}
	
	// Test for FavoritesMovieDatabase class
	@Test
	public void favoriteMovieDatabaseTest() throws SQLException {
		
		FavoriteMoviesDatabase favoriteMoviesDatabase = new FavoriteMoviesDatabase();

		assertFalse(favoriteMoviesDatabase.checkMovieInFavorites(movieTitle));
	}
}