package week6.assignment.main;

import java.util.Scanner;

import week6.assignment.factory.Movies;
import week6.assignment.factory.MoviesFactory;
import week6.assignment.model.Favorites;

public class MainClass {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		while(true) {
			
			System.out.println("Enter Your Choice");

			System.out.println("1 => Show All Coming soon Movies");
			System.out.println("2 => Show All Movies in Theaters");
			System.out.println("3 => Add Movies To Favorites");
			System.out.println("4 => Delete Movies from Favorites");
			System.out.println("5 => Show All Favorite Movies");
			System.out.println("0 => Exit");
			
			int choice = sc.nextInt();
			
			if(choice == 0) {
				break;
			}
			
			switch (choice) {
			
				case 1:
					Movies comingSoonMovies = MoviesFactory.getTheMovies(choice);
					comingSoonMovies.getMovies();
					break;
					
				case 2:
					Movies theaterMovies = MoviesFactory.getTheMovies(choice);
					theaterMovies.getMovies();
					break;
					
				case 3:
					Favorites favoriteMovies = (Favorites) MoviesFactory.getTheMovies(choice);
					favoriteMovies.addMoviesToFavorites();
					
					break;
					
				case 4:
					Favorites deleteFavoriteMovies = (Favorites) MoviesFactory.getTheMovies(choice);
					boolean isDeleted = deleteFavoriteMovies.deleteFavoriteMovies();
					if(isDeleted) {
						deleteFavoriteMovies.getMovies();
					}else {
						System.out.println("Movie is not Deleted");
					}
					break;
					
				case 5:
					Movies allFavoriteMovies = MoviesFactory.getTheMovies(choice);
					allFavoriteMovies.getMovies();
					break;
					
				default:System.out.println("Invalid Choice");
					break;
			}
		}
	}

}
