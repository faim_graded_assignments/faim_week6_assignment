package week6.assignment.factory;

import week6.assignment.model.ComingSoon;
import week6.assignment.model.Favorites;
import week6.assignment.model.TheaterMovies;

public class MoviesFactory{
	
	public static Movies getTheMovies(int movieChoice) {
		
		if(movieChoice == 1) {
			return new ComingSoon();
		}else if(movieChoice == 2) {
			return new TheaterMovies();
		}else {
			return new Favorites();
		}
	}
}
