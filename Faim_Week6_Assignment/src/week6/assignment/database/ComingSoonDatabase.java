package week6.assignment.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import week6.assignment.connection.DBConnection;
import week6.assignment.model.ComingSoon;

public class ComingSoonDatabase {
	
	private static Connection connection = DBConnection.getConnection();
	
	public List<ComingSoon> getAllComingSoonMovies() throws SQLException{
		
		List<ComingSoon> comingSoonMovies = new ArrayList<>();
		
		String query = "select * from comingsoon;";
		Statement statement = connection.createStatement();
		
		ResultSet resultSet = statement.executeQuery(query);
		while(resultSet.next()) {
			
			try {
				ComingSoon comingSoon = new ComingSoon();
				comingSoon.setId(resultSet.getInt(1));
				comingSoon.setTitle(resultSet.getString(2));
				comingSoon.setYear(resultSet.getInt(3));
				comingSoon.setGenre(resultSet.getString(4));
				comingSoon.setContentRating(resultSet.getString(5));
				comingSoon.setDuration(resultSet.getString(6));
				comingSoon.setReleaseDate(resultSet.getString(7));
				comingSoon.setActors(resultSet.getString(8));
				comingSoon.setImdbRating(resultSet.getString(9));
				
				comingSoonMovies.add(comingSoon);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			
		}
		return comingSoonMovies;
	}

}
