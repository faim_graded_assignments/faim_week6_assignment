package week6.assignment.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import week6.assignment.connection.DBConnection;
import week6.assignment.model.TheaterMovies;

public class TheaterMoviesDatabase {
	
	private static Connection connection = DBConnection.getConnection();
	
	public List<TheaterMovies> getAllTheaterMovies() throws SQLException{
		
		List<TheaterMovies> theaterMovies = new ArrayList<>();
		
		String query = "select * from theater;";
		Statement statement = connection.createStatement();
		
		ResultSet resultSet = statement.executeQuery(query);
		while(resultSet.next()) {
			
			try {
				TheaterMovies theatermovies = new TheaterMovies();
				theatermovies.setId(resultSet.getInt(1));
				theatermovies.setTitle(resultSet.getString(2));
				theatermovies.setYear(resultSet.getInt(3));
				theatermovies.setGenre(resultSet.getString(4));
				theatermovies.setContent_rating(resultSet.getString(5));
				theatermovies.setDuration(resultSet.getString(6));
				theatermovies.setRelease_date(resultSet.getString(7));
				theatermovies.setActors(resultSet.getString(8));
				theatermovies.setImdb_rating(resultSet.getString(9));
				
				theaterMovies.add(theatermovies);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			
		}
		return theaterMovies;
	}

}
