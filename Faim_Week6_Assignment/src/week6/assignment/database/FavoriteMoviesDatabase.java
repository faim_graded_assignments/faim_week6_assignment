package week6.assignment.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import week6.assignment.connection.DBConnection;
import week6.assignment.model.ComingSoon;
import week6.assignment.model.Favorites;

public class FavoriteMoviesDatabase {

	private static Connection connection = DBConnection.getConnection();

	public List<Favorites> getAllFavoriteMovies() throws SQLException{

		List<Favorites> favoriteMovies = new ArrayList<>();

		String query = "select * from favorites;";
		Statement statement = connection.createStatement();

		ResultSet resultSet = statement.executeQuery(query);
		while(resultSet.next()) {


			Favorites favorites = new Favorites();
			favorites.setId(resultSet.getInt(1));
			favorites.setTitle(resultSet.getString(2));
			favorites.setYear(resultSet.getInt(3));
			favorites.setGenre(resultSet.getString(4));
			favorites.setContentRating(resultSet.getString(5));
			favorites.setDuration(resultSet.getString(6));
			favorites.setReleaseDate(resultSet.getString(7));
			favorites.setActors(resultSet.getString(8));
			favorites.setImdbRating(resultSet.getString(9));

			favoriteMovies.add(favorites);


		}
		return favoriteMovies;
	}

	public boolean checkMovieInFavorites(String title) throws SQLException {

		String query = "select title from favorites where title like '" + title + "%';";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		if(resultSet.next()) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean selectMoviesFromComingSoon(String title) throws SQLException {
		
		PreparedStatement statement = null;
		String query = "select * from comingsoon where title like '" + title + "%';";
		String insertQuery = "insert into favorites values(?,?,?,?,?,?,?,?,?)";
		statement = connection.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery(query);
		if(resultSet.next()) {
			
			statement = connection.prepareStatement(insertQuery);
			statement.setInt(1,resultSet.getInt(1));
			statement.setString(2,resultSet.getString(2));
			statement.setInt(3,resultSet.getInt(3));
			statement.setString(4,resultSet.getString(4));
			statement.setString(5,resultSet.getString(5));
			statement.setString(6,resultSet.getString(6));
			statement.setString(7,resultSet.getString(7));
			statement.setString(8,resultSet.getString(8));
			statement.setString(9,resultSet.getString(9));
			statement.executeUpdate();
			return true;
		}else {
			return false;
		}
	}
	
	public boolean selectMoviesFromTheater(String title) throws SQLException {
		
		PreparedStatement statement = null;
		String query = "select * from theater where title like '" + title + "%';";
		String insertQuery = "insert into favorites values(?,?,?,?,?,?,?,?,?)";
		statement = connection.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery(query);
		if(resultSet.next()) {
			
			statement = connection.prepareStatement(insertQuery);
			statement.setInt(1,resultSet.getInt(1));
			statement.setString(2,resultSet.getString(2));
			statement.setInt(3,resultSet.getInt(3));
			statement.setString(4,resultSet.getString(4));
			statement.setString(5,resultSet.getString(5));
			statement.setString(6,resultSet.getString(6));
			statement.setString(7,resultSet.getString(7));
			statement.setString(8,resultSet.getString(8));
			statement.setString(9,resultSet.getString(9));
			statement.executeUpdate();
			return true;
		}else {
			return false;
		}
	}
	
	
	public boolean deleteMoviesFromFavorites(String title) throws SQLException {

		PreparedStatement statement = null;
		String query = "select * from favorites where title like '" + title + "%';";
		String deleteQuery = "delete from favorites where title like '" + title + "%';";
		statement = connection.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery(query);
		if(resultSet.next()) {
			statement = connection.prepareStatement(deleteQuery);
			statement.executeUpdate();
			return true;
		}else {
			return false;
		}
	}
}
