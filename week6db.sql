create table comingsoon(
	id int(10) primary key,
	title varchar(50),
	year int(4),
	genre varchar(15),
	content_rating varchar(10),
	duration varchar(10),
	release_date varchar(10),
	actors varchar(20),
	imdb_rating varchar(15)
);

insert into comingsoon values
(1,'Game Night',2018,'Action','11','PT100M','2018-02-28','Rachel McAdams','NA'),
(2,'Area X: Annihilation',2018,'Adventure','R','NA','2018-02-23','Tessa Thompson','NA'),
(3,'Hannah',2017,'Drama','NA','PT95M','2018-01-24','Charlotte Rampling','6.5'),
(4,'The Lodgers',2017,'Horror','R','PT92M','2018-03-09','Charlotte Vega','5.8'),
(5,'Beast of BUrsen',2018,'Crime','R','NA','2018-02','Daniel Radcliffe','NA'),
(6,'The Chamber',2016,'Thriller','NA','PT88M','2017-03-10','Charlotte Salt','4.4'),
(7,'Survivors Guide to Prison',2018,'Documentry','NA','PT102M','2018-02-23','Susan Sarandon','NA'),
(8,'Red Sparrow',2018,'Mystery','R','PT139M','2018-03-02','Jennifer Lawrence','NA');

create table theaeter(
	id int(10) primary key,
	title varchar(50),
	year int(4),
	genre varchar(15),
	content_rating varchar(10),
	duration varchar(10),
	release_date varchar(10),
	actors varchar(20),
	imdb_rating varchar(15)
);

insert into theatermovies values
(1,'Black Panther',2018,'Adventure','15','PT134M','2018-02-14','Chadwick Boseman','7.0'),
(2,'Grottmannen Dug',2018,'Animation','PG','PT89M','2018-03-23','Tom Hiddleston','6.3'),
(3,'Aiyaary',2018,'Action','NA','PT157M','2018-02-16','Sidharth Malhotra','NA'),
(4,'Samson',2018,'Drama','PG-13','NA','2018-02-16','Billy Zane','5.0'),
(5,'Loveless',2017,'Drama','R','PT127M",'2017-06-01','Maryana Spivak','7.8');

create table favorites(
	id int(10) ,
	title varchar(50) primary key,
	year int(4),
	genre varchar(15),
	content_rating varchar(10),
	duration varchar(10),
	release_date varchar(10),
	actors varchar(20),
	imdb_rating varchar(15)
);
